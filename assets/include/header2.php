</head>
<body>
	<div class="header">
		<div class="c-header1">
			<div class="l-container">
				<p class="c-header1__content">有資格者がご自宅に訪問し、身体介護や生活援助をおこなうサービスを提供しております。その他福祉と医療に関するご相談も無料で承ります。</p>
			</div>
		</div>
		<div class="c-header2">
			<div class="l-container">
				<div class="c-logo">
					<img src="../assets/image/common/logo.png" alt="">
				</div>
				<div class="c-header2__info">
					<div class="c-header2__info__content">
						<p>お気軽にお問い合わせください。</p>
					</div>
					<div class="c-header2__info__contact">
						<img src="../assets/image/common/icon_phone_yellow.png" alt="">
						<div class="c-header2__info__contact__detail">
							<p class="c-header2__info__contact__detail__time">平日 9:00～18:00</p>
							<p class="c-header2__info__contact__detail__phone">03-6379-5484</p>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="c-gnav">
			<div class="l-container">
				<ul>
					<li><a href="index.php"><img src="../assets/image/common/icon_home.png" alt=""></a></li>
					<li><a href="news/index.php">お知らせ</a></li>
					<li><a href="teamsakata/index.php">株式会社<br/>team sakata</a></li>
					<li><a href="shien/index.php">一般社団法人<br/>SHIEN</a></li>
					<li><a href="teamshien/index.php">一般社団法人<br/>team shien</a></li>
					<li><a href="sakura/index.php">NPO法人<br/>ライフサポートさくら</a></li>
					<li><a href="company/index.php">会社案内</a></li>
					<div class="clear"></div>
				</ul>
			</div>
		</div>
	</div>