<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<!--Main title-->
<div class="c-maintitle1">
	<img src="../assets/image/company/main_title.png" alt="">
	<div class="c-maintitle1__content">
		<p class="c-maintitle1__content__big">会社案内</p>
		<p class="c-maintitle1__content__small">Company Data</p>
	</div>
</div>
<!--End Main title-->

<!--Breadcrumbs-->
<div class="c-breadcrumbs">
	<div class="l-container">
		<a href="#">HOME</a>
		<p>会社案内</p>
	</div>
</div>
<!--End Breadcrumbs-->
<div class="p-company">
	<div class="p-company1">
		<div class="l-container">
			<div class="c-nav1">
				<div class="c-nav1__item">
					<p class="c-nav1__item__content">株式会社team sakata</p>
				</div>
				<div class="c-nav1__item">
					<p class="c-nav1__item__content">一般社団法人SHIEN</p>
				</div>
				<div class="c-nav1__item">
					<p class="c-nav1__item__content">一般社団法人team shien</p>
				</div>
				<div class="c-nav1__item">
					<p class="c-nav1__item__content">NPO法人ライフサポートさくら</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="p-company2">
		<div class="l-container">
			<section>
				<div class="c-title3">
					<img src="../assets/image/company/icon_title_company1.png" alt="">
					<p>株式会社team sakata</p>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>運営主体</th>
							<td>株式会社team sakata</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒156-0042　東京都世田谷区羽根木1-13-9-201</td>
						</tr>
						<tr>
							<th>代表者名</th>
							<td>阪田　祐治</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>03-3323-2495</td>
						</tr>
						<tr>
							<th>FAX</th>
							<td>03-3327-7166</td>
						</tr>
						<tr>
							<th>設立</th>
							<td></td>
						</tr>
					</table>
				</div>
			</section>
			<section>
				<div class="c-title3">
					<img src="../assets/image/company/icon_title_company2.png" alt="">
					<p>一般社団法人SHIEN</p>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>運営主体</th>
							<td>一般社団法人SHIEN</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒156-0042　東京都世田谷区羽根木1-13-9-201</td>
						</tr>
						<tr>
							<th>代表者名</th>
							<td>阪田　祐治</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>03-3323-2495</td>
						</tr>
						<tr>
							<th>FAX</th>
							<td>03-3327-7166</td>
						</tr>
						<tr>
							<th>設立</th>
							<td></td>
						</tr>
					</table>
				</div>
			</section>
			<section>
				<div class="c-title3">
					<img src="../assets/image/company/icon_title_company3.png" alt="">
					<p>一般社団法人team shien</p>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>運営主体</th>
							<td>一般社団法人team shien</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒156-0042　東京都世田谷区羽根木1-13-9-201</td>
						</tr>
						<tr>
							<th>代表者名</th>
							<td>南大路　直子</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>03-6379-5484</td>
						</tr>
						<tr>
							<th>FAX</th>
							<td>03-3327-7166</td>
						</tr>
						<tr>
							<th>設立</th>
							<td></td>
						</tr>
					</table>
				</div>
			</section>
			<section>
				<div class="c-title3">
					<img src="../assets/image/company/icon_title_company4.png" alt="">
					<p>NPO法人ライフサポートさくら</p>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>運営主体</th>
							<td>NPO法人ライフサポートさくら</td>
						</tr>
						<tr>
							<th>所在地</th>
							<td>〒154-0021　東京都世田谷区豪徳寺1-21-5 ヴィラ・ゴートク1-E</td>
						</tr>
						<tr>
							<th>代表者名</th>
							<td>阪田　祐治</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>03-3439-3977</td>
						</tr>
						<tr>
							<th>FAX</th>
							<td>03-3439-3978</td>
						</tr>
						<tr>
							<th>設立</th>
							<td></td>
						</tr>
					</table>
				</div>
			</section>
		</div>
	</div>
	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
