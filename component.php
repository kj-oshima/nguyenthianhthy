<!--Main title-->
<div class="c-maintitle1">
	<img src="../assets/image/news/main_title.png" alt="">
	<div class="c-maintitle1__content">
		<p class="c-maintitle1__content__big">お知らせ</p>
		<p class="c-maintitle1__content__small">News</p>
	</div>
</div>
<!--End Main title-->
<br>
<br>
<br>
<!--Breadcrumbs-->
<div class="c-breadcrumbs">
	<div class="l-container">
		<a href="#">HOME</a>
		<p>お知らせ</p>
	</div>
</div>
<!--End Breadcrumbs-->
<br>
<br>
<br>
<!--Main visual-->
<div class="c-mainvisual">
	<img src="../assets/image/top/main-visual.png" alt="">
</div>
<!--End visual-->
<br>
<br>
<br>

<div class="l-container">
	<!--Title-->
	<div class="c-title1">
		<p class="c-title1__big">チームエガオグループ（T.E.G Team Egao Group）</p>
		<p class="c-title1__sub">About</p>
	</div>

	<br>
	<br>
	<br>

	<div class="c-title2">
		<p>ホームページをリニューアルしました。</p>
	</div>

	<br>
	<br>
	<br>

	<div class="c-title3">
		<img src="../assets/image/company/icon_title_company1.png" alt="">
		<p>株式会社team sakata</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title3">
		<img src="../assets/image/company/icon_title_company2.png" alt="">
		<p>一般社団法人SHIEN</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title3">
		<img src="../assets/image/company/icon_title_company3.png" alt="">
		<p>一般社団法人team shien</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title3">
		<img src="../assets/image/company/icon_title_company4.png" alt="">
		<p>NPO法人ライフサポートさくら</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title4">
		<p>介護保険ケアプラン（事業所番号 1371212207）</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title5">
		<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
		<p>team shien</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title6">
		<p>成年後見制度とは</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title7">
		<p>YYYY/MM/DD</p>
	</div>
	<br>
	<br>
	<br>
	<div class="c-title100">
		<div class="c-title1">
			<p class="c-title1__big">お知らせ</p>
			<p class="c-title1__sub">News</p>
		</div>
		<div class="c-btn1">
			<a href="#">Facebook</a>
		</div>
	</div>
	<!--End Title-->
	<br>
	<br>
	<br>
	<!--table1-->
	<div class="c-table1">
		<table>
			<tr>
				<th>運営主体</th>
				<td>株式会社team sakata</td>
			</tr>
			<tr>
				<th>所在地</th>
				<td>〒156-0042　東京都世田谷区羽根木1-13-9-201</td>
			</tr>
			<tr>
				<th>代表者名</th>
				<td>阪田　祐治</td>
			</tr>
			<tr>
				<th>TEL</th>
				<td>03-3323-2495</td>
			</tr>
			<tr>
				<th>FAX</th>
				<td>03-3327-7166</td>
			</tr>
			<tr>
				<th>設立</th>
				<td></td>
			</tr>
		</table>
	</div>
	<!--end table 1-->
	<br>
	<br>
	<br>
	<!--table2-->
	<div class="c-table1 c-table1--color2">
		<table>
			<tr>
				<th>電話番号</th>
				<td>03-6379-5484</td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td>irishcafe@ab.auone-net.jp</td>
			</tr>
		</table>
	</div>
	<!--End table 2-->
	<br>
	<br>
	<br>
	<!--c-list1-->
	<div class="c-list1">
		<li>京王線「代田橋」駅　徒歩6分</li>
		<li>京王井の頭線「新代田」駅　徒歩9分</li>
		<li>京王井の頭線「東松原」駅   徒歩9分</li>
	</div>
	<!--end c-list1-->
	<br>
	<br>
	<br>
	<!--c-list2-->
	<div class="c-list2">
		<li>2017/11/25 <span>イベント情報ページを更新しました。</span></li>
		<li>2017/10/20 <span>イベント情報ページを更新しました。</span></li>
		<li>2017/09/25 <span>ホームページを開設いたしました。今後とも、チームエガオグループをよろしくお願いいたします。</span></li>
	</div>
	<!--end c-list2-->
	<br>
	<br>
	<br>
	<!--Info1-->
	<div class="c-info1">
		<p class="c-info1__title">このような法人様におすすめ</p>
		<li><a>介護事業の事が全くわからない。</a></li>
		<li><a>一人では不安だから一緒に事業計画から安定運営までサポートして欲しい。</a></li>
		<li><a>障害福祉サービスを取り組みたい。</a></li>
		<li><a>更生保護事業に興味がある。</a></li>
	</div>
	<div class="clear"></div>
	<!--End info1-->
	<br>
	<br>
	<br>
	<!--Info 2-->
	<div class="c-info2">
		<p class="c-info2__title">お問い合わせ先</p>
		<p class="c-info2__content">毎週水曜日10:00～13:00 / TEL.03-3439-3977</p>
	</div>
	<div class="clear"></div>
	<div class="c-info2 c-info2--color2">
		<p class="c-info2__title">お問い合わせ先</p>
		<p class="c-info2__content">月～金曜日 9:00～18:00 / TEL.03-6379-5484</p>
	</div>
	<div class="clear"></div>
	<!--End info2-->
	<br>
	<br>
	<br>
	<!--Info3-->
	<div class="c-info3">
		<div class="c-info3__left">
			<h2 class="c-info3__left__title__big">チームエガオグループで一緒に働きませんか？</h2>
			<p class="c-info3__left__title__small">ご興味のある方はお電話にてご連絡ください。</p>
			<div class="c-info3__left__contact">
				<img src="assets/image/top/icon_phone_white.png" alt="">
				<div class="c-info3__left__contact__info">
					<p class="c-info3__left__contact__info__time">平日 9:00～18:00</p>
					<p class="c-info3__left__contact__info__phone">03-6379-5484</p>
				</div>
			</div>
		</div>
		<div class="c-info3__right">
			<div class="c-info3__right__info">
				<img src="assets/image/top/icon_info_1.png" alt="">
				<p class="c-info3__right__info__content">株式会社team sakata</p>
			</div>
			<div class="c-info3__right__info">
				<img src="assets/image/top/icon_info_2.png" alt="">
				<p class="c-info3__right__info__content">一般社団法人SHIEN</p>
			</div>
			<div class="c-info3__right__info">
				<img src="assets/image/top/icon_info_3.png" alt="">
				<p class="c-info3__right__info__content">一般社団法人team shien</p>
			</div>
			<div class="c-info3__right__info">
				<img src="assets/image/top/icon_info_4.png" alt="">
				<p class="c-info3__right__info__content">NPO法人 ライフサポートさくら</p>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<!--End Info3-->
	<br>
	<br>
	<br>

	<!--map1-->
	<div class="c-map1">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.975896384098!2d139.6455380655739!3d35.65296548912843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f39e89d04f03%3A0xee26a0f25f9b58ab!2zSmFwYW4sIOOAkjE1NC0wMDIxIFTFjWt5xY0tdG8sIFNldGFnYXlhLWt1LCBHxY10b2t1amksIDEgQ2hvbWXiiJIyMeKIkjUsIOODtOOCo-ODqeOCtOODvOODiOOCrw!5e0!3m2!1sen!2s!4v1507704035249" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
	<!--end map1-->

	<br>
	<br>
	<br>
	
	<!--map Block-->
	<div class="c-mapBlock">
		<div class="c-map1">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.975896384098!2d139.6455380655739!3d35.65296548912843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f39e89d04f03%3A0xee26a0f25f9b58ab!2zSmFwYW4sIOOAkjE1NC0wMDIxIFTFjWt5xY0tdG8sIFNldGFnYXlhLWt1LCBHxY10b2t1amksIDEgQ2hvbWXiiJIyMeKIkjUsIOODtOOCo-ODqeOCtOODvOODiOOCrw!5e0!3m2!1sen!2s!4v1507704035249" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="c-mapBlock__link">
			<a href="#">詳しい地図を見る</a>
		</div>
		<div class="c-list1">
			<li>京王線「代田橋」駅　徒歩6分</li>
			<li>京王井の頭線「新代田」駅　徒歩9分</li>
			<li>京王井の頭線「東松原」駅   徒歩9分</li>
		</div>
		<div class="c-mapBlock__info">
			 <p>※家庭料理のおざわが一階にあります。</p>
		</div>
	</div>
	<!--End map block-->

	<br>
	<br>
	<br>
	
	<!--nav 1-->
	<div class="c-nav1">
		<div class="c-nav1__item">
			<p class="c-nav1__item__content">株式会社team sakata</p>
		</div>
		<div class="c-nav1__item">
			<p class="c-nav1__item__content">一般社団法人SHIEN</p>
		</div>
		<div class="c-nav1__item">
			<p class="c-nav1__item__content">一般社団法人team shien</p>
		</div>
		<div class="c-nav1__item">
			<p class="c-nav1__item__content">NPO法人ライフサポートさくら</p>
		</div>
		<div class="clear"></div>
	</div>
	<!--end nav 1-->
	<br>
	<br>
	<br>
	<!--nav 2-->
	<div class="c-nav2">
		<div class="c-nav2__item">
			<img src="assets/image/top/nav2_1.png" alt="">
			<div class="c-nav2__item__info">
				<img src="assets/image/top/icon_nav2_1.png" alt="">
				<p>株式会社team sakata</p>
			</div>
		</div>
		<div class="c-nav2__item">
			<img src="assets/image/top/nav2_2.png" alt="">
			<div class="c-nav2__item__info">
				<img src="assets/image/top/icon_nav2_2.png" alt="">
				<p>一般社団法人SHIEN</p>
			</div>
		</div>
		<div class="c-nav2__item">
			<img src="assets/image/top/nav2_3.png" alt="">
			<div class="c-nav2__item__info">
				<img src="assets/image/top/icon_nav2_3.png" alt="">
				<p>一般社団法人team shien</p>
			</div>
		</div>
		<div class="c-nav2__item">
			<img src="assets/image/top/nav2_4.png" alt="">
			<div class="c-nav2__item__info">
				<img src="assets/image/top/icon_nav2_4.png" alt="">
				<p>NPO法人<br/>
				ライフサポートさくら</p>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<!--End nav 2-->
	<br>
	<br>
	<br>
	<!--go top-->
	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
	<!--end gotop-->

	<br>
	<br>
	<br>

	<!--btn1-->
	<div class="c-btn1">
		<a href="#">Facebook</a>
	</div>
	<!--end btn1-->

	<br>
	<br>
	<br>

	<!--text-->
	<div class="c-text1">
		<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。</p>
	</div>
	<!--end text-->

	<br>
	<br>
	<br>

	<!--entry-->
	<div class="c-entry1">
		<div class="c-entry1__text">
			<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
			<p>画像を1枚挿入できます。</p>
		</div>
		<div class="c-entry1__img">
			<img src="../assets/image/news/entry1.png" alt="">
		</div>
		<div class="clear"></div>
	</div>
	<!--end entry-->
	<br>
	<br>
	<br>

	<div class="u-bg1">
		<br>
		<br>
		<br>
	</div>
	
	<br>
	<br>
	<br>

	<div class="u-bg2">
		<br>
		<br>
		<br>
	</div>
	
	<br>
	<br>
	<br>

	<div class="l-flame1">
		<div class="l-flame1__left">
			<p class="l-flame1__left__info">看護師・社会福祉士・精神保健福祉士・介護福祉士の資格を持った福祉のスペシャリストが<br/>
			介護保険利用のためのケアプランを作成を承ります。</p>
			<p class="l-flame1__left__info">介護保険の利用に関わらず、福祉医療全般のご相談を無料にて承ります。</p>
			<div class="c-info2">
				<p class="c-info2__title">お問い合わせ先</p>
				<p class="c-info2__content">月～金曜日 9:00～18:00 / TEL.03-6379-4343</p>
			</div>
		</div>
		<div class="l-flame1__right">
			<img src="../assets/image/teamshien/flame1.png" alt="">
		</div>
		<div class="clear"></div>
	</div>

	<br>
	<br>
	<br>

	<div class="l-flame2">
		
	</div>
</div>
