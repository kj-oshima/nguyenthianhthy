<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>


<!--Main title-->
<div class="c-maintitle1">
	<img src="../assets/image/news/main_title.png" alt="">
	<div class="c-maintitle1__content">
		<p class="c-maintitle1__content__big">お知らせ</p>
		<p class="c-maintitle1__content__small">News</p>
	</div>
</div>
<!--End Main title-->

<!--Breadcrumbs-->
<div class="c-breadcrumbs">
	<div class="l-container">
		<a href="#">HOME</a>
		<p>お知らせ</p>
	</div>
</div>
<!--End Breadcrumbs-->
<div class="p-news">
	<div class="p-news1">
		<div class="l-container">
			<section>
				<div class="c-title2">
					<p>ホームページをリニューアルしました。</p>
				</div>
				<div class="c-title7">
					<p>YYYY/MM/DD</p>
				</div>
				<div class="c-entry1">
					<div class="c-entry1__text">
						<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
						<p>画像を1枚挿入できます。</p>
					</div>
					<div class="c-entry1__img">
						<img src="../assets/image/news/entry1.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</section>
			<section>
				<div class="c-title2">
					<p>ホームページをリニューアルしました。</p>
				</div>
				<div class="c-title7">
					<p>YYYY/MM/DD</p>
				</div>
				<div class="c-entry1">
					<div class="c-entry1__text">
						<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
						<p>画像を1枚挿入できます。</p>
					</div>
					<div class="c-entry1__img">
						<img src="../assets/image/news/entry1.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</section>
			<section>
				<div class="c-title2">
					<p>ホームページをリニューアルしました。</p>
				</div>
				<div class="c-title7">
					<p>YYYY/MM/DD</p>
				</div>
				<div class="c-entry1">
					<div class="c-entry1__text">
						<p>本文を入力します。お客様自身で、編集・更新することができます。</p>
						<p>画像を1枚挿入できます。</p>
					</div>
					<div class="c-entry1__img">
						<img src="../assets/image/news/entry1.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</section>
		</div>
	</div>
	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
