<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<!--Main title-->
<div class="c-maintitle1">
	<img src="../assets/image/sakura/main_title.png" alt="">
	<div class="c-maintitle1__content">
		<p class="c-maintitle1__content__big">NPO法人ライフサポートさくら</p>
		<p class="c-maintitle1__content__small">About</p>
	</div>
</div>
<!--End Main title-->

<!--Breadcrumbs-->
<div class="c-breadcrumbs">
	<div class="l-container">
		<a href="#">HOME</a>
		<p>NPO法人ライフサポートさくら</p>
	</div>
</div>
<!--End Breadcrumbs-->

<div class="p-sakura">
	<div class="p-sakura1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>介護予防・日常生活総合支援　～住民主体型・地域デイサービス～</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>地域デイサービス　（　さくらとゆかいな仲間たち　）</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info">みなさんで一緒にごはんを作って食べて、おしゃべりやちょっとした運動をして楽しい時間を一緒に過ごしませんか？</p>
						<div class="c-title6">
							<p>参加対象者</p>
						</div>
						<p class="l-flame1__left__info">要支援１・２及びチェックリスト該当者（65歳以上）</p>
						<div class="c-title6">
							<p>参加料</p>
						</div>
						<p class="l-flame1__left__info">食事代：500円（イベントの材料費別途徴収）</p>
						<div class="c-info2">
							<p class="c-info2__title">お問い合わせ先</p>
							<p class="c-info2__content">毎週水曜日10:00～13:00 / TEL.03-3439-3977</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/sakura/p-sakura1.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-sakura2">
		<div class="l-flame2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<p>ノーマライゼーションギャラリー</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>team さくら</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info">障害の有無を意識しなくても生活ができる社会の実現を目指す考え方を共有するフリースペースです。</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/sakura/p-sakura2.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-sakura3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>お問い合わせ先</p>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>電話番号</th>
							<td>03-3323-2495</td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td>team-sakata@solid.ocn.ne.jp</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="p-sakura4">
		<div class="l-flame2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<p>求人案内</p>
				</div>
				<div class="c-text1">
					<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。</p>
				</div>
			</div>
		</div>
	</div>
	<div class="p-sakura5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>アクセスマップ</p>
				</div>
				<div class="c-mapBlock">
					<div class="c-map1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.975896384098!2d139.6455380655739!3d35.65296548912843!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f39e89d04f03%3A0xee26a0f25f9b58ab!2zSmFwYW4sIOOAkjE1NC0wMDIxIFTFjWt5xY0tdG8sIFNldGFnYXlhLWt1LCBHxY10b2t1amksIDEgQ2hvbWXiiJIyMeKIkjUsIOODtOOCo-ODqeOCtOODvOODiOOCrw!5e0!3m2!1sen!2s!4v1507704035249" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="c-mapBlock__link">
						<a href="#">詳しい地図を見る</a>
					</div>
					<div class="c-list1">
						<li>小田急小田原線「豪徳寺」駅　徒歩2分</li>
						<li>東急世田谷線「山下」駅　徒歩3分</li>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
