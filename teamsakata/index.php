<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<!--Main title-->
<div class="c-maintitle1">
	<img src="../assets/image/teamsakata/main-title.png" alt="">
	<div class="c-maintitle1__content">
		<p class="c-maintitle1__content__big">株式会社team sakata</p>
		<p class="c-maintitle1__content__small">About</p>
	</div>
</div>
<!--End Main title-->

<!--Breadcrumbs-->
<div class="c-breadcrumbs">
	<div class="l-container">
		<a href="#">HOME</a>
		<p>株式会社team sakata</p>
	</div>
</div>
<!--End Breadcrumbs-->

<div class="p-teamsakata">
	<div class="p-teamsakata1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>総合社会福祉プロデュース</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>事務所設立～運営計画</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info">事業計画の立案から、採用、育成、利用者の獲得まで、事業が安定稼働までをサポートいたし<br>ます。異業種・未経験からの参入でも、しっかりとサポートいたします。</p>
						<div class="c-info1">
							<p class="c-info1__title">このような法人様におすすめ</p>
							<li><a>介護事業の事が全くわからない。</a></li>
							<li><a>一人では不安だから一緒に事業計画から安定運営までサポートして欲しい。</a></li>
							<li><a>障害福祉サービスを取り組みたい。</a></li>
							<li><a>更生保護事業に興味がある。</a></li>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamsakata/p-teamsakata1.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamsakata2">
		<div class="l-flame2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<p>心理カウンセリング</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>アイリッシュカフェ「心のオアシス」</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info"><span>1人で抱えて、考えているだけでは堂々めぐり・・・</span><br>
							どうしていいのかわからない・・・<br>
						落ち込んでいく・・・</p>
						<div class="c-info2 c-info2--color2">
							<p class="c-info2__title">私たちは疲れた心に潤いを与え、明日への活力を！！<br>
								そして、今後の人生の活力を養うお手伝いをします。<br>
							それが・・・心のオアシス</p>
							<p class="c-info2__content"><img src="../assets/image/teamsakata/info2.png" alt=""></p>
							<div class="clear"></div>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamsakata/p-teamsakata2.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamsakata3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>介護保険ケアプラン（事業所番号 1371212207）</p>
				</div>
				<div class="c-table1">
					<table>
						<tr>
							<th>電話番号</th>
							<td>03-3323-2495</td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td>team-sakata@w6.dion.ne.jp</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamsakata4">
		<div class="l-flame2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<p>求人案内</p>
				</div>
				<div class="c-text1">
					<p>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。テキストを入力することができます。<br/>テキストを入力することができます。テキストを入力することができます。</p>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamsakata5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>アクセスマップ</p>
				</div>
				<div class="c-mapBlock">
					<div class="c-map1">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342836!2d139.65736581557408!3d35.66677693837113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1507703936042" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="c-mapBlock__link">
						<a href="#">詳しい地図を見る</a>
					</div>
					<div class="c-list1">
						<li>京王線「代田橋」駅　徒歩6分</li>
						<li>京王井の頭線「新代田」駅　徒歩9分</li>
						<li>京王井の頭線「東松原」駅   徒歩9分</li>
					</div>
					<div class="c-mapBlock__info">
						<p>※家庭料理のおざわが一階にあります。</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
