<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header1.php'); ?>
<title></title>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header2.php'); ?>

<!--Main title-->
<div class="c-maintitle1">
	<img src="../assets/image/teamshien/main_title.png" alt="">
	<div class="c-maintitle1__content">
		<p class="c-maintitle1__content__big">一般社団法人team shien</p>
		<p class="c-maintitle1__content__small">About</p>
	</div>
</div>
<!--End Main title-->

<!--Breadcrumbs-->
<div class="c-breadcrumbs">
	<div class="l-container">
		<a href="#">HOME</a>
		<p>一般社団法人team shien</p>
	</div>
</div>
<!--End Breadcrumbs-->
<div class="p-teamshien">
	<div class="p-teamshien1">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>介護保険ケアプラン（事業所番号 1371212207）</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>team shien</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info">看護師・社会福祉士・精神保健福祉士・介護福祉士の資格を持った福祉のスペシャリストが<br/>
						介護保険利用のためのケアプランを作成を承ります。</p>
						<p class="l-flame1__left__info">介護保険の利用に関わらず、福祉医療全般のご相談を無料にて承ります。</p>
						<div class="c-info2">
							<p class="c-info2__title">お問い合わせ先</p>
							<p class="c-info2__content">月～金曜日 9:00～18:00 / TEL.03-6379-4343</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/flame1.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamshien2">
		<div class="l-flame2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<p>訪問介護（事業所番号 1371209469）</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>ちーむしえんあどぼかしー</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info">介護員がお宅を訪問してサービスを提供させて頂きます。介護保険外の自費サービスも行って<br>おります。<br>まずは、お気軽にお問い合わせください。</p>
						<div class="c-info2 c-info2--color2">
							<p class="c-info2__title">お問い合わせ先</p>
							<p class="c-info2__content">月～金曜日 9:00～18:00 / TEL.03-6379-5484</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/p-teamshien2.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamshien3">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>一般相談・特定相談支援（事業所番号 1331202471）</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>team shien m.a</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<div class="c-title6">
							<p>地域移行<br>地域定着</p>
						</div>
						<p class="l-flame1__left__info">障害をお持ちの方で、長期入院・入所をなさっている方の地域生活への移行・定着をお手伝い<br>しています。<br>※地域の行政担当・病院・施設の相談員の方を通じてお問い合わせください。</p>
						<p class="l-flame1__left__info">障害福祉サービスの利用を希望されている方のサービス等利用計画作成を承ります。<br>
						例）訪問介護の利用や、就労支援先への通所など</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/p-teamshien3.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamshien2">
		<div class="l-flame2 u-bg1">
			<div class="l-container">
				<div class="c-title4">
					<p>居宅介護・移動支援（事業所番号 1311202467）</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>team shien m.a</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<p class="l-flame1__left__info">ちーむしえんあどぼかしーの訪問介護同様、専門の知識を有した介護員がケアに伺います。<br>
						まずは、お気軽にお問い合わせください。</p>
						<p class="l-flame1__left__info">障害福祉サービス受給者証をお持ちの方は、介護給付費の１割負担でご利用いただけます。<br>
						（所得に応じて負担上限金額が異なります）</p>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/p-teamshien4.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamshien5">
		<div class="l-flame2">
			<div class="l-container">
				<div class="c-title4">
					<p>一般相談・特定相談支援（事業所番号 1331202471）</p>
				</div>
				<div class="c-title5">
					<img src="../assets/image/teamshien/icon_title_teamshien1.png" alt="">
					<p>ちーむしえん 研修部</p>
				</div>
				<div class="l-flame1">
					<div class="l-flame1__left">
						<div class="c-title6">
							<p>介護職員初任者研修<br>
							同行援護従事者研修</p>
						</div>
						<p class="l-flame1__left__info">社会人でも取得しやすいプログラム、通信制を導入しております。<br>
							当社講師が誠心誠意、次世代の福祉人材を育て、社会へ貢献いたします。<br>
							受講される方の意向を反映したオーダーメイド制研修を実施。<br>
						6名以上の人数が揃い次第、随時研修を開始いたします。</p>
						<div class="c-info2">
							<p class="c-info2__title">お問い合わせ先</p>
							<p class="c-info2__content">毎週水曜日10:00～13:00 / TEL.03-3439-3977</p>
						</div>
					</div>
					<div class="l-flame1__right">
						<img src="../assets/image/teamshien/p-teamshien5.png" alt="">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="p-teamshien6 u-bg1">
		<div class="l-container">
			<div class="c-title4">
				<p>お問い合わせ先</p>
			</div>
			<div class="c-table1 c-table1--color2">
				<table>
					<tr>
						<th>電話番号</th>
						<td>03-6379-5484</td>
					</tr>
					<tr>
						<th>メールアドレス</th>
						<td>irishcafe@ab.auone-net.jp</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="p-teamshien7">
		<div class="l-container">
			<div class="c-title4">
				<p>求人案内</p>
			</div>
			<div class="p-teamshien7__info">
				<p>随時、スタッフを募集しております。詳細については、お電話にてお尋ねください。</p>
				<p>【募集職種】　<br>
					①ホームヘルパー<br>
					②ケアマネージャー<br>
				③相談支援専門員</p>
			</div>
		</div>
	</div>
	<div class="p-teamshien8 u-bg1">
		<div class="l-container">
			<div class="c-title4">
				<p>アクセスマップ</p>
			</div>
			<div class="c-mapBlock">
				<div class="c-map1">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.4152130342836!2d139.65736581557408!3d35.66677693837113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f30e11f17561%3A0x9bb452e4b9254772!2s1+Chome-13-9+Hanegi%2C+Setagaya-ku%2C+T%C5%8Dky%C5%8D-to+156-0042%2C+Japan!5e0!3m2!1sen!2s!4v1507703936042" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="c-mapBlock__link">
					<a href="#">詳しい地図を見る</a>
				</div>
				<div class="c-list1">
					<li>京王線「代田橋」駅　徒歩6分</li>
					<li>京王井の頭線「新代田」駅　徒歩9分</li>
					<li>京王井の頭線「新代田」駅　徒歩9分</li>
				</div>
				<div class="c-mapBlock__info">
					<p>※家庭料理のおざわが一階にあります。</p>
				</div>
			</div>
		</div>
	</div>
	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
