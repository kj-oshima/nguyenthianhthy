<div class="p-top">
	<div class="p-top1">
		<div class="c-mainvisual">
			<img src="assets/image/top/main-visual.png" alt="">
		</div>
	</div>
	<div class="p-top2">
		<div class="l-container">
			<div class="c-title100">
				<div class="c-title1">
					<p class="c-title1__big">お知らせ</p>
					<p class="c-title1__sub">News</p>
				</div>
				<div class="c-btn1">
					<a href="#">Facebook</a>
				</div>
			</div>
			<div class="c-list2">
				<li>2017/11/25 <span>イベント情報ページを更新しました。</span></li>
				<li>2017/10/20 <span>イベント情報ページを更新しました。</span></li>
				<li>2017/09/25 <span>ホームページを開設いたしました。今後とも、チームエガオグループをよろしくお願いいたします。</span></li>
			</div>
		</div>
	</div>
	<div class="p-top3">
		<div class="l-container">
			<div class="c-title1">
				<p class="c-title1__big">チームエガオグループ（T.E.G Team Egao Group）</p>
				<p class="c-title1__sub">About</p>
			</div>
			<div class="c-nav2">
				<div class="c-nav2__item">
					<img src="assets/image/top/nav2_1.png" alt="">
					<div class="c-nav2__item__info">
						<img src="assets/image/top/icon_nav2_1.png" alt="">
						<p>株式会社team sakata</p>
					</div>
				</div>
				<div class="c-nav2__item">
					<img src="assets/image/top/nav2_2.png" alt="">
					<div class="c-nav2__item__info">
						<img src="assets/image/top/icon_nav2_2.png" alt="">
						<p>一般社団法人SHIEN</p>
					</div>
				</div>
				<div class="c-nav2__item">
					<img src="assets/image/top/nav2_3.png" alt="">
					<div class="c-nav2__item__info">
						<img src="assets/image/top/icon_nav2_3.png" alt="">
						<p>一般社団法人team shien</p>
					</div>
				</div>
				<div class="c-nav2__item">
					<img src="assets/image/top/nav2_4.png" alt="">
					<div class="c-nav2__item__info">
						<img src="assets/image/top/icon_nav2_4.png" alt="">
						<p>NPO法人<br/>
						ライフサポートさくら</p>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="p-top4">
		<div class="l-container">
			<div class="c-info3">
				<div class="c-info3__left">
					<h2 class="c-info3__left__title__big">チームエガオグループで一緒に働きませんか？</h2>
					<p class="c-info3__left__title__small">ご興味のある方はお電話にてご連絡ください。</p>
					<div class="c-info3__left__contact">
						<img src="assets/image/top/icon_phone_white.png" alt="">
						<div class="c-info3__left__contact__info">
							<p class="c-info3__left__contact__info__time">平日 9:00～18:00</p>
							<p class="c-info3__left__contact__info__phone">03-6379-5484</p>
						</div>
					</div>
				</div>
				<div class="c-info3__right">
					<div class="c-info3__right__info">
						<img src="assets/image/top/icon_info_1.png" alt="">
						<p class="c-info3__right__info__content">株式会社team sakata</p>
					</div>
					<div class="c-info3__right__info">
						<img src="assets/image/top/icon_info_2.png" alt="">
						<p class="c-info3__right__info__content">一般社団法人SHIEN</p>
					</div>
					<div class="c-info3__right__info">
						<img src="assets/image/top/icon_info_3.png" alt="">
						<p class="c-info3__right__info__content">一般社団法人team shien</p>
					</div>
					<div class="c-info3__right__info">
						<img src="assets/image/top/icon_info_4.png" alt="">
						<p class="c-info3__right__info__content">NPO法人 ライフサポートさくら</p>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>

	<div class="c-gotop">
		<a onclick="topFunction()" id="gotop" title="Go to top"><img src="../assets/image/common/gotop.png" alt=""></a>

		<script>
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("gotop").style.display = "block";
				} else {
					document.getElementById("gotop").style.display = "none";
				}
			}

			function topFunction() {
				document.body.scrollTop = 0;
				document.documentElement.scrollTop = 0;
			}
		</script>
	</div>
</div>
